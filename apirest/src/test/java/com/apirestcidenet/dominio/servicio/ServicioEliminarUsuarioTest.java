package com.apirestcidenet.dominio.servicio;

import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.mock;

public class ServicioEliminarUsuarioTest {

    private static final String PRIMER_APELLIDO = "GONZALEZ2";
    private static final String SEGUNDO_APELLIDO = "MELGAREJO";
    private static final String PRIMER_NOMBRE = "DIEGO";
    private static final String OTROS_NOMBRE = "ALIRIO";
    private static final String PAIS = "co";
    private static final String TIPO_IDENTIFICACION = "cc";
    private static final String NUMERO_IDENTIFICACION = "1090504046";
    private static final String CORREO = "diego.gonzalez@cidenet.co";
    private static final String FECHA_INGRESO = "29/01/2021";
    private static final String AREA = "fi";
    private static final Boolean ESTADO = true;
    private static final String FECHAREGISTRO = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    private static final String FORMATO_VALIDO_FECHA_INGRESO = "dd/MM/yyyy";
    private static final String FORMATO_VALIDO_FECHA_REGISTRO = "dd/MM/yyyy HH:mm:ss";
    private UsuarioPuerto usuarioPuerto;
    private static final String USUARIO_ELIMINADO = "Usuario Eliminado";
    private static final Long IDELIMINAR=1l;
    @Before
    public void setUp(){
        this.usuarioPuerto=mock(UsuarioPuerto.class);
    }


    @Test
    public void deberiaEliminarUsuario(){
     ServicioEliminarUsuario servicioEliminarUsuario=new ServicioEliminarUsuario(this.usuarioPuerto);
     String mensaje=servicioEliminarUsuario.eliminarUsuario(IDELIMINAR);
        Assert.assertEquals(USUARIO_ELIMINADO,mensaje);

    }
}
