package com.apirestcidenet.dominio.servicio;

import com.apirestcidenet.aplicacion.transformador.UsuarioTransformador;
import com.apirestcidenet.constructor.UsuarioConstructor;
import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.mock;

public class ServicioCrearUsuarioTest {

    private static final String PRIMER_APELLIDO = "GONZALEZ2";
    private static final String SEGUNDO_APELLIDO = "MELGAREJO";
    private static final String PRIMER_NOMBRE = "DIEGO";
    private static final String OTROS_NOMBRE = "ALIRIO";
    private static final String PAIS = "co";
    private static final String TIPO_IDENTIFICACION = "cc";
    private static final String NUMERO_IDENTIFICACION = "1090504046";
    private static final String CORREO = "diego.gonzalez@cidenet.co";
    private static final String FECHA_INGRESO = "29/01/2021";
    private static final String AREA = "fi";
    private static final Boolean ESTADO = true;
    private static final String FECHAREGISTRO = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    private static final String FORMATO_VALIDO_FECHA_INGRESO = "dd/MM/yyyy";
    private static final String FORMATO_VALIDO_FECHA_REGISTRO = "dd/MM/yyyy HH:mm:ss";
    private UsuarioPuerto usuarioPuerto;
    private static  final String REGISTRO_EXITOSO="Registro Exitoso";
    @Before
    public void setUp(){
        this.usuarioPuerto=mock(UsuarioPuerto.class);
    }

    @Test
    public void deberiaCrearUnUsuario(){
        UsuarioConstructor usuarioConstructor=new UsuarioConstructor();
        Usuario usuario = UsuarioTransformador.crearUsuario(usuarioConstructor.construirUsuarioDto());
        ServicioCrearUsuario servicioCrearUsuario = new ServicioCrearUsuario(this.usuarioPuerto);
        String respuesta= servicioCrearUsuario.crearPago(usuario);
        Assert.assertEquals(REGISTRO_EXITOSO,respuesta);

    }
}
