package com.apirestcidenet.constructor;

import com.apirestcidenet.aplicacion.dto.UsuarioDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UsuarioConstructor {
    private static final String PRIMER_APELLIDO = "GONZALEZ";
    private static final String SEGUNDO_APELLIDO = "MELGAREJO";
    private static final String PRIMER_NOMBRE = "DIEGO";
    private static final String OTROS_NOMBRE = "ALIRIO";
    private static final String PAIS = "co";
    private static final String TIPO_IDENTIFICACION = "cc";
    private static final String NUMERO_IDENTIFICACION = "1090504046";
    private static final String CORREO = "diego.gonzalez@cidenet.co";
    private static final String FECHA_INGRESO = "29/01/2021";
    private static final String AREA = "fi";
    private static final Boolean ESTADO = true;
    private static final String FECHAREGISTRO = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    private static final String FORMATO_VALIDO_FECHA_INGRESO = "dd/MM/yyyy";
    private static final String FORMATO_VALIDO_FECHA_REGISTRO = "dd/MM/yyyy HH:mm:ss";
    private String primerApellido;
    private String segundoApellido;
    private String primerNombre;
    private String OtrosNombres;
    private String pais;
    private String tipoIdentificacion;
    private String NumeroIdentificacion;
    private String correo;
    private String fechaIngreso;
    private String area;
    private Boolean estado;
    private String fechaRegistro;

    public UsuarioConstructor(String primerApellido, String segundoApellido, String primerNombre, String otrosNombres, String pais, String tipoIdentificacion, String numeroIdentificacion, String correo, String fechaIngreso, String area, Boolean estado, String fechaRegistro) {
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.primerNombre = primerNombre;
        OtrosNombres = otrosNombres;
        this.pais = pais;
        this.tipoIdentificacion = tipoIdentificacion;
        NumeroIdentificacion = numeroIdentificacion;
        this.correo = correo;
        this.fechaIngreso = fechaIngreso;
        this.area = area;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;

    }

    public UsuarioConstructor() {
        this.primerApellido = PRIMER_APELLIDO;
        this.segundoApellido = SEGUNDO_APELLIDO;
        this.primerNombre = PRIMER_NOMBRE;
        OtrosNombres = OTROS_NOMBRE;
        this.pais = PAIS;
        this.tipoIdentificacion = TIPO_IDENTIFICACION;
        NumeroIdentificacion = NUMERO_IDENTIFICACION;
        this.correo = CORREO;
        this.fechaIngreso = FECHA_INGRESO;
        this.area = AREA;
        this.estado = ESTADO;
        this.fechaRegistro = FECHAREGISTRO;
    }

    public UsuarioConstructor setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
        return this;
    }

    public UsuarioConstructor setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
        return this;
    }

    public UsuarioConstructor setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
        return this;
    }

    public UsuarioConstructor setOtrosNombres(String otrosNombres) {
        this.OtrosNombres = otrosNombres;
        return this;
    }

    public UsuarioConstructor setPais(String pais) {
        this.pais = pais;
        return this;
    }

    public UsuarioConstructor setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
        return this;
    }

    public UsuarioConstructor setNumeroIdentificacion(String numeroIdentificacion) {
        this.NumeroIdentificacion = numeroIdentificacion;
        return this;
    }

    public UsuarioConstructor setCorreo(String correo) {
        this.correo = correo;
        return this;
    }

    public UsuarioConstructor setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
        return this;
    }

    public UsuarioConstructor setArea(String area) {
        this.area = area;
        return this;
    }

    public UsuarioConstructor setEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public UsuarioConstructor setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
        return this;
    }
  public UsuarioDto construirUsuarioDto(){
        UsuarioDto usuarioDto = new UsuarioDto(this.primerApellido,
                this.segundoApellido,this.primerNombre,this.OtrosNombres,
                this.pais,this.tipoIdentificacion,this.NumeroIdentificacion,this.area);
        usuarioDto.setCorreo(this.correo);
        usuarioDto.setEstado(this.estado?"activo":"inactivo");
        usuarioDto.setFechaIngreso(this.fechaIngreso);
        usuarioDto.setFechaRegistro(this.fechaRegistro);
        return usuarioDto;
  }


}
