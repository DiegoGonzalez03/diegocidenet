package com.apirestcidenet.infraestructura.controlador;

import com.apirestcidenet.aplicacion.dto.UsuarioDto;
import com.apirestcidenet.constructor.UsuarioConstructor;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import com.apirestcidenet.infraestructura.controlador.dto.RespuestaDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class UsuarioControladorTest {

    private static final String PRIMER_APELLIDO = "GONZALEZ2";
    private static final String SEGUNDO_APELLIDO = "MELGAREJO";
    private static final String PRIMER_NOMBRE = "DIEGO";
    private static final String OTROS_NOMBRE = "ALIRIO";
    private static final String PAIS = "co";
    private static final String TIPO_IDENTIFICACION = "cc";
    private static final String NUMERO_IDENTIFICACION = "1090504046";
    private static final String CORREO = "diego.gonzalez@cidenet.co";
    private static final String FECHA_INGRESO = "29/01/2021";
    private static final String AREA = "fi";
    private static final Boolean ESTADO = true;
    private static final String FECHAREGISTRO = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    private static final String FORMATO_VALIDO_FECHA_INGRESO = "dd/MM/yyyy";
    private static final String FORMATO_VALIDO_FECHA_REGISTRO = "dd/MM/yyyy HH:mm:ss";
    private UsuarioPuerto usuarioPuerto;
    private static final String REGISTRO_EXITOSO = "Registro Exitoso";
    private static final Long ELIMINARID=1L;
    private static final String USUARIO_ELIMINADO = "Usuario Eliminado";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void deberiaCrearUnUsuario() throws Exception {
        UsuarioConstructor usuarioConstructor = new UsuarioConstructor();
        UsuarioDto usuarioDto = usuarioConstructor.construirUsuarioDto();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/usuario/")
                .content(objectMapper.writeValueAsString(usuarioDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        String jsonRespuesta = mvcResult.getResponse().getContentAsString();
        RespuestaDto respuestaDto = objectMapper.readValue(jsonRespuesta, RespuestaDto.class);
        Assert.assertEquals(REGISTRO_EXITOSO, respuestaDto.getMensajeRespuesta());

    }
    @Test
    public  void deberiaEliminarUnUsuario() throws  Exception{
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .delete("/usuario/"+ELIMINARID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        String jsonRespuesta = mvcResult.getResponse().getContentAsString();
        RespuestaDto respuestaDto = objectMapper.readValue(jsonRespuesta, RespuestaDto.class);
        Assert.assertEquals(USUARIO_ELIMINADO, respuestaDto.getMensajeRespuesta());
    }
}
