package com.apirestcidenet.aplicacion.dto;

import com.apirestcidenet.aplicacion.transformador.UsuarioTransformador;
import com.apirestcidenet.constructor.UsuarioConstructor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class UsuarioDtoTest {
    private static final String PRIMER_APELLIDO = "GONZALEZ2";
    private static final String SEGUNDO_APELLIDO = "MELGAREJO";
    private static final String PRIMER_NOMBRE = "DIEGO";
    private static final String OTROS_NOMBRE = "ALIRIO";
    private static final String PAIS = "co";
    private static final String TIPO_IDENTIFICACION = "cc";
    private static final String NUMERO_IDENTIFICACION = "1090504046";
    private static final String CORREO = "diego.gonzalez@cidenet.co";
    private static final String FECHA_INGRESO = "29/01/2021";
    private static final String AREA = "fi";
    private static final Boolean ESTADO = true;
    private static final String FECHAREGISTRO = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    private static final String FORMATO_VALIDO_FECHA_INGRESO = "dd/MM/yyyy";
    private static final String FORMATO_VALIDO_FECHA_REGISTRO = "dd/MM/yyyy HH:mm:ss";
    private UsuarioTransformador usuarioTransformador;

    @Before
    public void configurar() {
        this.usuarioTransformador = new UsuarioTransformador();

    }

    @Test
    public void deberCrearUsuarioObjeto() {
        UsuarioConstructor usuarioConstructor = new UsuarioConstructor();
        UsuarioDto usuarioDto = usuarioConstructor.setPrimerApellido(PRIMER_APELLIDO).construirUsuarioDto();
        Assert.assertEquals(PRIMER_APELLIDO, usuarioDto.getPrimerApellido());
        Assert.assertEquals(SEGUNDO_APELLIDO, usuarioDto.getSegundoApellido());
        Assert.assertEquals(PRIMER_NOMBRE, usuarioDto.getPrimerNombre());
        Assert.assertEquals(OTROS_NOMBRE, usuarioDto.getOtrosNombres());
        Assert.assertEquals(PAIS, usuarioDto.getPais());
        Assert.assertEquals(TIPO_IDENTIFICACION, usuarioDto.getTipoIdentificacion());
        Assert.assertEquals(NUMERO_IDENTIFICACION, usuarioDto.getNumeroIdentificacion());
        Assert.assertEquals(CORREO, usuarioDto.getCorreo());
        Assert.assertEquals(FECHA_INGRESO, usuarioDto.getFechaIngreso());

    }

    @Test
    public void deberiaCrearListaUsuarioDtoObjecto() {
        UsuarioConstructor usuarioConstructor = new UsuarioConstructor();
        List<UsuarioDto> usuarioDtos = List.of(usuarioConstructor.setPrimerApellido(PRIMER_APELLIDO).construirUsuarioDto());
        Assert.assertEquals(PRIMER_APELLIDO, usuarioDtos.get(0).getPrimerApellido());
        Assert.assertEquals(SEGUNDO_APELLIDO, usuarioDtos.get(0).getSegundoApellido());
        Assert.assertEquals(PRIMER_NOMBRE, usuarioDtos.get(0).getPrimerNombre());
        Assert.assertEquals(OTROS_NOMBRE, usuarioDtos.get(0).getOtrosNombres());
        Assert.assertEquals(PAIS, usuarioDtos.get(0).getPais());
        Assert.assertEquals(TIPO_IDENTIFICACION, usuarioDtos.get(0).getTipoIdentificacion());
        Assert.assertEquals(NUMERO_IDENTIFICACION, usuarioDtos.get(0).getNumeroIdentificacion());
        Assert.assertEquals(CORREO, usuarioDtos.get(0).getCorreo());
        Assert.assertEquals(FECHA_INGRESO, usuarioDtos.get(0).getFechaIngreso());

    }
}
