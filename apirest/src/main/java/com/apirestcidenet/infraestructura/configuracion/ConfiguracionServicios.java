package com.apirestcidenet.infraestructura.configuracion;

import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import com.apirestcidenet.dominio.servicio.ServicioActualizarUsuario;
import com.apirestcidenet.dominio.servicio.ServicioCrearUsuario;
import com.apirestcidenet.dominio.servicio.ServicioEliminarUsuario;
import com.apirestcidenet.dominio.servicio.ServicioPaginarUsuario;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfiguracionServicios {



    @Bean
    public ServicioCrearUsuario servicioCrearUsuario(UsuarioPuerto usuarioPuerto){
        return  new ServicioCrearUsuario(usuarioPuerto);
    }
    @Bean
    public ServicioPaginarUsuario servicioPaginarUsuario(UsuarioPuerto usuarioPuerto) {
        return new ServicioPaginarUsuario(usuarioPuerto);
    }
    @Bean
    public ServicioEliminarUsuario servicioEliminarUsuario(UsuarioPuerto usuarioPuerto) {
        return new ServicioEliminarUsuario(usuarioPuerto);
    }
    @Bean
    public ServicioActualizarUsuario servicioActualizarUsuario(UsuarioPuerto usuarioPuerto){
        return new ServicioActualizarUsuario(usuarioPuerto);
    }
}
