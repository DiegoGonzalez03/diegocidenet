package com.apirestcidenet.infraestructura.error;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MensajeError {
    private String mensaje;

    public MensajeError(String mensaje) {
        this.mensaje = mensaje;
    }
}
