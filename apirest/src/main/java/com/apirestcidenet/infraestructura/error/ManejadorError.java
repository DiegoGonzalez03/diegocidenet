package com.apirestcidenet.infraestructura.error;

import com.apirestcidenet.dominio.excepcion.UsuarioExcepcion;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ManejadorError extends ResponseEntityExceptionHandler {
    private  static final String MENSAJE_ERROR="Hubo un error,por favor contacte al administrador";
    private static  final Map<String ,Integer> RESPUESTAS_ESTADO_HTTP= new HashMap<>();

    public ManejadorError() {

        RESPUESTAS_ESTADO_HTTP.put(UsuarioExcepcion.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<MensajeError> mostrarExcepcion(Exception exception){
        ResponseEntity<MensajeError> resultado;
        String nombreExcepcion=exception.getClass().getSimpleName();
        String mensaje=exception.getMessage();
        Integer codigo= RESPUESTAS_ESTADO_HTTP.get(nombreExcepcion);
        if(codigo!=null){
            MensajeError mensajeError=new MensajeError(mensaje);
            resultado=new ResponseEntity<>(mensajeError,HttpStatus.valueOf(codigo));
        }else{
            MensajeError mensajeError=new MensajeError(MENSAJE_ERROR);
            resultado=new ResponseEntity<>(mensajeError,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resultado;
    }
}
