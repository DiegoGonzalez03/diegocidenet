package com.apirestcidenet.infraestructura.controlador.dto;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@Getter
public class RespuestaListaDto<T> implements Serializable {

    private List<T> lista;

    public RespuestaListaDto(List<T> lista) {
        this.lista = lista;
    }

}
