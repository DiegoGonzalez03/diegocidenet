package com.apirestcidenet.infraestructura.controlador.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class RespuestaDto implements Serializable {

    private String mensajeRespuesta;

    public RespuestaDto() {
    }

    public RespuestaDto(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }
}
