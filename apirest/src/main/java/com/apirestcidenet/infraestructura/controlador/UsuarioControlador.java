package com.apirestcidenet.infraestructura.controlador;

import com.apirestcidenet.aplicacion.dto.PaginacionUsuarioDto;
import com.apirestcidenet.aplicacion.dto.UsuarioDto;
import com.apirestcidenet.aplicacion.manejador.ManejadorActualizarUsuario;
import com.apirestcidenet.aplicacion.manejador.ManejadorCrearUsuario;
import com.apirestcidenet.aplicacion.manejador.ManejadorEliminarUsuario;
import com.apirestcidenet.aplicacion.manejador.ManejadorPaginarUsuario;
import com.apirestcidenet.infraestructura.controlador.dto.RespuestaDto;
import com.apirestcidenet.infraestructura.controlador.dto.RespuestaListaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
@CrossOrigin("http://localhost:4200")
public class UsuarioControlador {

    private final ManejadorCrearUsuario manejadorCrearUsuario;
    private final ManejadorPaginarUsuario manejadorPaginarUsuario;
    private final ManejadorEliminarUsuario manejadorEliminarUsuario;
    private final ManejadorActualizarUsuario manejadorActualizarUsuario;
    private static final Logger logger = LoggerFactory.getLogger(UsuarioControlador.class);
    private static final String LOG_CREAR_USUARIOS = "Realizo peticion a Crear usuarios";
    private static final String LOG_LISTAR_USUARIOS = "Realizo peticion a Listar usuarios";
    private static final String LOG_ELIMINAR_USUARIOS = "Realizo peticion a Eliminar usuarios";
    private static final String LOG_ACTUALIZAR_USUARIOS = "Realizo peticion a Actualizar usuarios";

    public UsuarioControlador(ManejadorCrearUsuario manejadorCrearUsuario, ManejadorPaginarUsuario manejadorPaginarUsuario, ManejadorEliminarUsuario manejadorEliminarUsuario, ManejadorActualizarUsuario manejadorActualizarUsuario) {
        this.manejadorCrearUsuario = manejadorCrearUsuario;
        this.manejadorPaginarUsuario = manejadorPaginarUsuario;
        this.manejadorEliminarUsuario = manejadorEliminarUsuario;
        this.manejadorActualizarUsuario = manejadorActualizarUsuario;
    }

    @PostMapping("/")
    public ResponseEntity<RespuestaDto> crearUsuario(@RequestBody UsuarioDto usuarioDto) {
        String respuesta = this.manejadorCrearUsuario.crearUsuario(usuarioDto);
        logger.info(LOG_CREAR_USUARIOS + " Creo->" + usuarioDto.toString());

        return new ResponseEntity<>(new RespuestaDto(respuesta), HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<PaginacionUsuarioDto> paginar(@RequestParam Integer paginaActual) {

        PaginacionUsuarioDto respuesta = this.manejadorPaginarUsuario.paginar(paginaActual);
        logger.info(LOG_LISTAR_USUARIOS + " Pagina" + paginaActual);
        return new ResponseEntity<>(respuesta, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RespuestaDto> eliminarUsuario(@PathVariable("id") Long id) {
        String respuesta = this.manejadorEliminarUsuario.eliminarUsuario(id);
        logger.info(LOG_ELIMINAR_USUARIOS);
        return new ResponseEntity<>(new RespuestaDto(respuesta), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RespuestaDto> actualizarUsuario(@PathVariable("id") Long id,
                                                          @RequestBody UsuarioDto usuarioDto) {
        String respuesta = this.manejadorActualizarUsuario.actualizarUsuario(id, usuarioDto);
        logger.info(LOG_ACTUALIZAR_USUARIOS + " -> " + usuarioDto.toString());
        return new ResponseEntity<>(new RespuestaDto(respuesta), HttpStatus.CREATED);
    }

}
