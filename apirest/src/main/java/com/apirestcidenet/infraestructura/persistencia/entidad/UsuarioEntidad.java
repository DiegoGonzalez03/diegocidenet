package com.apirestcidenet.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity(name = "usuario")
@Table(indexes = {@Index(name = "correo_index", columnList = "correo",unique = true)})

@Getter
public class UsuarioEntidad implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column
    private String primerApellido;
    @Column
    private String segundoApellido;
    @Column
    private String primerNombre;
    @Column
    private String OtrosNombres;
    @Column
    private String pais;
    @Column
    private String correo;
    @Column
    private LocalDate fechaIngreso;
    @Column
    private String area;
    @Column
    private Boolean estado;
    @Column
    private LocalDateTime fechaRegistro;
    @Column
    private LocalDateTime fechaEdicion;
    private String tipoDocumentoIdentificacion;
    private String numeroIdentificacion;
    public UsuarioEntidad(String primerApellido, String segundoApellido, String primerNombre, String otrosNombres, String pais, String tipoIdentificacion, String numeroIdentificacion, String correo, LocalDate fechaIngreso, String area, Boolean estado, LocalDateTime fechaRegistro) {
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.primerNombre = primerNombre;
        OtrosNombres = otrosNombres;
        this.pais = pais;
        this.tipoDocumentoIdentificacion=tipoIdentificacion;
        this.numeroIdentificacion=numeroIdentificacion;

        this.correo = correo;
        this.fechaIngreso = fechaIngreso;
        this.area = area;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public LocalDateTime getFechaEdicion() {
        return fechaEdicion;
    }

    public UsuarioEntidad() {

    }

    public void setFechaEdicion(LocalDateTime fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
