package com.apirestcidenet.infraestructura.persistencia.transformador;

import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.infraestructura.persistencia.entidad.UsuarioEntidad;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class UsuarioTransformadorPersistencia {

    public static Usuario crearUsuario(UsuarioEntidad usuarioEntidad) {
        Usuario usuario = new Usuario(usuarioEntidad.getPrimerApellido(), usuarioEntidad.getSegundoApellido(),
                usuarioEntidad.getPrimerNombre(), usuarioEntidad.getOtrosNombres(), usuarioEntidad.getPais(),
                usuarioEntidad.getTipoDocumentoIdentificacion(), usuarioEntidad.getNumeroIdentificacion(),
                usuarioEntidad.getArea(), usuarioEntidad.getFechaIngreso().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        usuario.setCorreo(usuarioEntidad.getCorreo());
        usuario.setId(usuarioEntidad.getId());
        usuario.setEstado(usuarioEntidad.getEstado());
        usuario.setFechaRegistro(usuarioEntidad.getFechaRegistro().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        if (usuarioEntidad.getFechaEdicion() != null)
            usuario.setFechaEdicion(usuarioEntidad.getFechaEdicion().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));

        return usuario;
    }

    public static UsuarioEntidad crearUsuarioEntidad(Usuario usuario) {
        LocalDate fechaIngreso = LocalDate.parse(usuario.getFechaIngreso(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDateTime fechaRegistro = LocalDateTime.parse(usuario.getFechaRegistro(), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        UsuarioEntidad usuarioEntidad = new UsuarioEntidad(usuario.getPrimerApellido(), usuario.getSegundoApellido(),
                usuario.getPrimerNombre(), usuario.getOtrosNombres(),
                usuario.getPais(), usuario.getTipoIdentificacion(), usuario.getNumeroIdentificacion(), usuario.getCorreo(), fechaIngreso,
                usuario.getArea(), usuario.getEstado(), fechaRegistro);
        if(usuario.getFechaEdicion()!=null){
            LocalDateTime fechaEdicion = LocalDateTime.parse(usuario.getFechaEdicion(), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        usuarioEntidad.setFechaEdicion(fechaEdicion);
        }
        if (usuario.getId() != null) {
            usuarioEntidad.setId(usuario.getId());
        }
        return usuarioEntidad;
    }

    public static List<Usuario> crearListaUsuarios(List<UsuarioEntidad> usuarios) {
        List<Usuario> usuarios1 = new ArrayList<>();
        usuarios.forEach((final UsuarioEntidad usuarioEntidad) -> usuarios1.add(crearUsuario(usuarioEntidad)));
        return usuarios1;
    }
}
