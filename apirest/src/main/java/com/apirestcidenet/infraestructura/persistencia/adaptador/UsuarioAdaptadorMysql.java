package com.apirestcidenet.infraestructura.persistencia.adaptador;

import com.apirestcidenet.dominio.Paginacion;
import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import com.apirestcidenet.infraestructura.persistencia.entidad.UsuarioEntidad;
import com.apirestcidenet.infraestructura.persistencia.transformador.UsuarioTransformadorPersistencia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UsuarioAdaptadorMysql implements UsuarioPuerto  {

    @PersistenceContext
    private EntityManager entityManager;
    private static final String CORREOS_REPETIDOS="SELECT u FROM usuario u WHERE " +
            "u.primerNombre = ?1 AND " +
            "u.primerApellido = ?2 ";
    private static final String BUSCARPORTIPOYNUMERO="SELECT u FROM usuario u WHERE " +
            "u.tipoDocumentoIdentificacion = ?1 AND " +
            "u.numeroIdentificacion = ?2 ";
    public String COUNT = "SELECT count(u) FROM usuario u";
    public String PAGINAR = "FROM usuario";
    public UsuarioAdaptadorMysql(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void crearUsuario(Usuario usuario) {
        UsuarioEntidad usuarioEntidad=UsuarioTransformadorPersistencia.crearUsuarioEntidad(usuario);
        this.entityManager.persist(usuarioEntidad);
    }

    @Override
    public int correosIguales(String primerNombre, String primerApellido) {
        Query query= this.entityManager.createQuery(CORREOS_REPETIDOS);
        query.setParameter(1,primerNombre);
        query.setParameter(2,primerApellido);

        return query.getResultList().size();
    }



    public Boolean buscarUsuarioPorTipoYNumeroIdentificacion(String tipo, String numero) {
        Query query= this.entityManager.createQuery(BUSCARPORTIPOYNUMERO);
        query.setParameter(1,tipo);
        query.setParameter(2,numero);
        int numeroUsuario=query.getResultList().size();


        return query.getResultList().size()>0?true:false;
    }

    @Override
    public int contar() {
        Query query = this.entityManager.createQuery(COUNT);
        Object obj = query.getSingleResult();
        if (obj != null) {
            int c=Integer.parseInt(obj.toString());
            return c;
        }

        return Integer.valueOf(0);
    }

    @Override
    public List<Usuario> paginar(Paginacion paginacion) {
        Query query = this.entityManager.createQuery(PAGINAR);
        query.setFirstResult(paginacion.getFilaInicial());
        query.setMaxResults(Paginacion.CANTIDAD_FILAS_PAGINA);
        List<UsuarioEntidad> usuarioEntidades = query.getResultList();
        return UsuarioTransformadorPersistencia.crearListaUsuarios(usuarioEntidades);
    }

    @Override
    public void eliminarUsuario(Usuario usuario) {
    UsuarioEntidad usuarioEntidad=UsuarioTransformadorPersistencia.crearUsuarioEntidad(usuario);
    this.entityManager.remove(this.entityManager.contains(usuarioEntidad)?usuarioEntidad:this.entityManager.merge(usuarioEntidad));
    //em.contains(entity) ? entity : em.merge(entity)
    }

    @Override
    public Usuario buscarPorId(Long id) {
        return UsuarioTransformadorPersistencia.crearUsuario(this.entityManager.find(UsuarioEntidad.class,id)) ;
    }

    @Override
    public void ActualizarUsuario(Usuario usuario) {
        UsuarioEntidad usuarioEntidad=UsuarioTransformadorPersistencia.crearUsuarioEntidad(usuario);
        this.entityManager.merge(usuarioEntidad);
    }


}
