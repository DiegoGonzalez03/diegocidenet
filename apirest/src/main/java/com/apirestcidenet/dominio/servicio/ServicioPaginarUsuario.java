package com.apirestcidenet.dominio.servicio;

import java.util.List;

import com.apirestcidenet.dominio.Paginacion;
import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;

public class ServicioPaginarUsuario {

    private final UsuarioPuerto usuarioPuerto;

    public ServicioPaginarUsuario(UsuarioPuerto usuarioPuerto) {
        this.usuarioPuerto = usuarioPuerto;
    }

    public List<Usuario> listar(Paginacion paginacion) {
        Integer cantidadFilasUsuario  = this.usuarioPuerto.contar();

        paginacion.calcularFilas(cantidadFilasUsuario);

        List<Usuario> usuarios = this.usuarioPuerto.paginar(paginacion);
        paginacion.getCantidadPaginas();

        return usuarios;

    }

}

