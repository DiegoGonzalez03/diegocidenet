package com.apirestcidenet.dominio.servicio;

import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import com.apirestcidenet.infraestructura.controlador.UsuarioControlador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public class ServicioEliminarUsuario {
    private final UsuarioPuerto usuarioPuerto;
    private static final Logger logger = LoggerFactory.getLogger(UsuarioControlador.class);
    private static final String USUARIO_NO_EXISTE = "El usuario no existe";
    private static final String USUARIO_ELIMINADO = "Usuario Eliminado";

    public ServicioEliminarUsuario(UsuarioPuerto usuarioPuerto) {
        this.usuarioPuerto = usuarioPuerto;
    }

    @Transactional
    public String eliminarUsuario(Long id) {
        Usuario usuario = this.usuarioPuerto.buscarPorId(id);
        if (usuario == null) {
            logger.error(USUARIO_NO_EXISTE + "-> " + id);
        }
        this.usuarioPuerto.eliminarUsuario(usuario);
        return USUARIO_ELIMINADO;
    }
}
