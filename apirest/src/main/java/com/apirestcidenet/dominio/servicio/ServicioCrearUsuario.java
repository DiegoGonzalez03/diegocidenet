package com.apirestcidenet.dominio.servicio;

import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.excepcion.UsuarioExcepcion;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import com.apirestcidenet.infraestructura.controlador.UsuarioControlador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServicioCrearUsuario {
    private final UsuarioPuerto usuarioPuerto;
    private static final String DOCUMENTACION_REPETIDA="El tipo y el documento de identificacion ya se encuentra registrado";
    private static final Logger logger = LoggerFactory.getLogger(UsuarioControlador.class);
    private static  final String REGISTRO_EXITOSO="Registro Exitoso";
    public ServicioCrearUsuario(UsuarioPuerto usuarioPuerto) {
        this.usuarioPuerto = usuarioPuerto;
    }

    public String crearPago(Usuario usuario){
        int correosiguales=this.usuarioPuerto.correosIguales(usuario.getPrimerNombre(),usuario.getPrimerApellido());
        String correo="";
        String dominio=usuario.getPais().equals("co")?".co":".us";
        String fechaRegistro= LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")).toString();
        Boolean usuarioRepetido=this.usuarioPuerto.buscarUsuarioPorTipoYNumeroIdentificacion(usuario.getTipoIdentificacion(),usuario.getNumeroIdentificacion());
        if(usuarioRepetido){
            logger.error(DOCUMENTACION_REPETIDA);
            throw new UsuarioExcepcion(DOCUMENTACION_REPETIDA);
        }

        usuario.setFechaRegistro(fechaRegistro);
        if(correosiguales>0){
            correo=usuario.getPrimerNombre()+usuario.getPrimerApellido().replace(" ","")+correosiguales+"@cidenet"+dominio;

        }else
            correo=usuario.getPrimerNombre()+usuario.getPrimerApellido().replace(" ","")+"@cidenet"+dominio;
       usuario.setCorreo(correo.toLowerCase());

        this.usuarioPuerto.crearUsuario(usuario);
      return REGISTRO_EXITOSO;
    }
    private Boolean validarFechaRegistro(String fechaRegistro){


        return (Integer.parseInt(fechaRegistro.split("/")[0])%2)==0;
    }
}
