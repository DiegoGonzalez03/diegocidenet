package com.apirestcidenet.dominio.servicio;

import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.excepcion.UsuarioExcepcion;
import com.apirestcidenet.dominio.puerto.UsuarioPuerto;
import com.apirestcidenet.infraestructura.controlador.UsuarioControlador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServicioActualizarUsuario {

    private final UsuarioPuerto usuarioPuerto;
    private static final Logger logger = LoggerFactory.getLogger(UsuarioControlador.class);
    private static final String IDENTIFICACION_REPETIDA = "Ese tipo y numero de identificacion ya se encuentra registrado";
    private static final String USUARIO_NO_EXISTE = "El usuario no existe";
    private static final String USUARIO_ACTUALIZADO = "Usuario Actualizado";

    public ServicioActualizarUsuario(UsuarioPuerto usuarioPuerto) {
        this.usuarioPuerto = usuarioPuerto;
    }

    @Transactional
    public String actualizarUsuario(Long id, Usuario usuario) {
        Usuario usuarioAntiguo = this.usuarioPuerto.buscarPorId(id);
        if(usuarioAntiguo==null){
            logger.error(USUARIO_NO_EXISTE+"-> " + id);
            throw new UsuarioExcepcion(USUARIO_NO_EXISTE);
        }
        if (!usuarioAntiguo.getTipoIdentificacion().equalsIgnoreCase(usuario.getTipoIdentificacion())
                && !usuarioAntiguo.getNumeroIdentificacion().equalsIgnoreCase(usuario.getNumeroIdentificacion()))
            if (validarIdentificacionExistente(usuario.getTipoIdentificacion(), usuario.getNumeroIdentificacion())) {
                logger.error(IDENTIFICACION_REPETIDA + "-> " + " usuario");
                throw new UsuarioExcepcion(IDENTIFICACION_REPETIDA);

            }
        if (!usuarioAntiguo.getPrimerApellido().equalsIgnoreCase(usuario.getPrimerApellido())
                || !usuarioAntiguo.getPrimerNombre().equalsIgnoreCase(usuario.getPrimerNombre()))
            usuario.setCorreo(renovarCorreo(usuario.getPrimerNombre(), usuario.getPrimerApellido(), usuario.getPais()));
        usuario.setId(usuarioAntiguo.getId());
        usuario.setFechaRegistro(usuarioAntiguo.getFechaRegistro());
        String fechaEdicion= LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")).toString();
        usuario.setFechaEdicion(fechaEdicion);
        this.usuarioPuerto.ActualizarUsuario(usuario);
        return USUARIO_ACTUALIZADO;
    }

    private boolean validarIdentificacionExistente(String tipoIdentificacion, String numeroIdentificacion) {
        return this.usuarioPuerto.buscarUsuarioPorTipoYNumeroIdentificacion(tipoIdentificacion, numeroIdentificacion);

    }

    private String renovarCorreo(String primerNombre, String primerApellido, String pais) {
        String correo = "";
        String dominio = pais.equals("co") ? ".co" : ".us";
        int correosiguales = this.usuarioPuerto.correosIguales(primerNombre, primerApellido);
        if (correosiguales > 0) {
            correo = primerNombre + primerApellido.replace(" ", "").toLowerCase() + correosiguales + "@cidenet" + dominio;

        } else
            correo = primerNombre + primerApellido.replace(" ", "").toLowerCase() + "@cidenet" + dominio;
        return correo;
    }
}
