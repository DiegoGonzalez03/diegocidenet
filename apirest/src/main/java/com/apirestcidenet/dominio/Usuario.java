package com.apirestcidenet.dominio;

import com.apirestcidenet.dominio.excepcion.UsuarioExcepcion;
import com.apirestcidenet.infraestructura.controlador.UsuarioControlador;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.regex.Pattern;

@Getter
public class Usuario {

    private Long id;
    private final String primerApellido;
    private final String segundoApellido;
    private final String primerNombre;
    private final String OtrosNombres;
    private final String pais;
    private final String tipoIdentificacion;
    private final String NumeroIdentificacion;
    private String correo;
    private final String fechaIngreso;
    private final String area;
    private Boolean estado;
    private String fechaRegistro;
    private String fechaEdicion;
    private static final String ERROR_FORMATO_PRIMERNOMBRE = "Error en el formato del Primer Nombre";
    private static final String ERROR_FORMATO_OTROSNOMBRES = "Error en el formato del Otros Nombres";
    private static final String ERROR_FORMATO_PRIMERAPELLIDO = "Error en el formato del Primer Apellido";
    private static final String ERROR_FORMATO_SEGUNDOAPELLIDO = "Error en el formato del Segundo Apellido";
    private static final String PAIS_NO_PERMITIDO = "El pais que digito no esta permitido";
    private static final String TIPO_IDENTIFICACION_NO_PERMITIDA = "Tipo de identificacion no permitida";
    private static final String FORMATO_DOCUMENTO_NO_PERMITIDO = "Formato de documento no permitido";
    private static final String FORMATO_FECHA_INVALIDO = "Formato de fecha de ingraso invalido";
    private static final String FECHA_MAYOR_ACTUAL = "Fecha de ingreso no debe ser mayor a la actual";
    private static final String FECHA_MENOR_UN_MES = "La fecha de ingresp no debe mer menor por mas de un mes al a fecha actual";
    private static final Logger logger = LoggerFactory.getLogger(UsuarioControlador.class);


    public Usuario(String primerApellido, String segundoApellido, String primerNombre, String otrosNombres, String pais, String tipoIdentificacion, String numeroIdentificacion, String area, String fechaIngreso) {
        this.excepcionesAlCreearUsuario(primerApellido, segundoApellido, primerNombre, otrosNombres, pais, tipoIdentificacion, numeroIdentificacion, fechaIngreso);
        this.validarFechaIngreso(fechaIngreso);
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.primerNombre = primerNombre;
        OtrosNombres = otrosNombres;
        this.pais = pais;
        this.tipoIdentificacion = tipoIdentificacion;
        NumeroIdentificacion = numeroIdentificacion;
        this.area = area;
        this.estado = true;
        this.fechaIngreso = fechaIngreso;

    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }


    public void setEstado(Boolean estado) {
        this.estado = estado;
    }


    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }


    private void validarFechaIngreso(String fechaIngreso) {
        Boolean formato = validarformatoFechaIngreso(fechaIngreso);
        if (!formato) {
            logger.error(FORMATO_FECHA_INVALIDO + "-> " + fechaIngreso);
            throw new UsuarioExcepcion(FORMATO_FECHA_INVALIDO);
        }
        LocalDate fechaauxiliar = LocalDate.parse(fechaIngreso, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        if (!fechaauxiliar.isBefore(LocalDate.now())) {
            logger.error(FECHA_MAYOR_ACTUAL + "-> " + fechaIngreso);
            throw new UsuarioExcepcion(FECHA_MAYOR_ACTUAL);
        }
        long meses = ChronoUnit.MONTHS.between(fechaauxiliar, LocalDate.now());

        if (meses > 1) {
            logger.error(FECHA_MENOR_UN_MES + "-> " + fechaIngreso);
            throw new UsuarioExcepcion(FECHA_MENOR_UN_MES);
        }
    }

    public void setId(Long id) {
        this.id = id;
    }


    private boolean validarNumeroDocumento(String documento) {
        String regexp = "^[A-Za-z0-9-]+$";
        return Pattern.matches(regexp, documento) && documento.length() <= 20;
    }


    private boolean validarformatoFechaIngreso(String fechaIngreso) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            df.parse(fechaIngreso);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }


    private boolean validarnombres(String nombres, int tamanio) {
        String regexp = "^[A-NO-Z]+(\\s*[A-NO-Z])[A-NO-Z]+$";
        return Pattern.matches(regexp, nombres) && nombres.length() <= tamanio;
    }


    private void excepcionesAlCreearUsuario(String primerApellido, String segudoApellido, String primerNombre, String otrosNombres, String pais, String tipoIdentificacion, String numeroIdentificacion, String fechaIngreso) {
        if (!this.validarnombres(primerApellido, 20)) {
            logger.error(ERROR_FORMATO_PRIMERAPELLIDO + "-> " + primerApellido);
            throw new UsuarioExcepcion(ERROR_FORMATO_PRIMERAPELLIDO + "-> " + primerApellido);
        }
        if (!this.validarnombres(segudoApellido, 20)) {
            logger.error(ERROR_FORMATO_SEGUNDOAPELLIDO + "-> " + segudoApellido);
            throw new UsuarioExcepcion(ERROR_FORMATO_SEGUNDOAPELLIDO);
        }
        if (!this.validarnombres(primerNombre, 20)) {
            logger.error(ERROR_FORMATO_PRIMERNOMBRE + "-> " + primerNombre);
            throw new UsuarioExcepcion(ERROR_FORMATO_PRIMERNOMBRE);
        }
        if (!this.validarnombres(otrosNombres, 50)) {
            logger.error(ERROR_FORMATO_OTROSNOMBRES + "-> " + otrosNombres);
            throw new UsuarioExcepcion(ERROR_FORMATO_OTROSNOMBRES);
        }
        if (!pais.equalsIgnoreCase("co") && !pais.equalsIgnoreCase("eu")) {
            logger.error(PAIS_NO_PERMITIDO);
            throw new UsuarioExcepcion(PAIS_NO_PERMITIDO + "-> " + pais);
        }
        if (!tipoIdentificacion.equalsIgnoreCase("cc") && !tipoIdentificacion.equalsIgnoreCase("ce") && !tipoIdentificacion.equalsIgnoreCase("pa")
                && !tipoIdentificacion.equalsIgnoreCase("pe")) {
            logger.error(TIPO_IDENTIFICACION_NO_PERMITIDA + "-> " + tipoIdentificacion);
            throw new UsuarioExcepcion(TIPO_IDENTIFICACION_NO_PERMITIDA);
        }
        if (!this.validarNumeroDocumento(numeroIdentificacion)) {
            logger.error(FORMATO_DOCUMENTO_NO_PERMITIDO + "-> " + numeroIdentificacion);
            throw new UsuarioExcepcion(FORMATO_DOCUMENTO_NO_PERMITIDO);

        }

    }

}
