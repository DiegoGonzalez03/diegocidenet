package com.apirestcidenet.dominio;

import com.apirestcidenet.dominio.excepcion.DataInvalidaException;
import lombok.Getter;

@Getter
public class Paginacion {
    public static final String LA_PAGINA_ACTUAL_NO_ES_VALIDA = "La pagina actual no es valida";
    public static final String LA_CANTIDAD_FILAS_NO_ES_VALIDA = "La cantidad de filas no es valida";
    public static final int CANTIDAD_FILAS_PAGINA = 10;

    private int paginaActual;
    private int cantidadPaginas;

    private int filaInicial;
    private int filaFinal;

    public Paginacion(Integer paginaActual) {
        this.validarPaginaActual(paginaActual);
        this.paginaActual = paginaActual.intValue();
    }


    public void calcularFilas(Integer cantidadFilasTotal) {
        this.validarCantidadFilas(cantidadFilasTotal);

        this.calcularFilaInicial();
        this.calcularFilaFinal(cantidadFilasTotal.intValue());
        this.calcularPaginas(cantidadFilasTotal.intValue());
    }

    private void calcularPaginas(int cantidadFilasTotal) {
        double resultado = ((double) cantidadFilasTotal) / ((double) CANTIDAD_FILAS_PAGINA);
        this.cantidadPaginas = (int) resultado;
        if(resultado - Math.floor(resultado) != 0) {
            this.cantidadPaginas ++;
        }


    }

    private void validarPaginaActual(Integer paginaActual) {
        if (paginaActual == null || paginaActual.intValue() < 0) {
            throw new DataInvalidaException(LA_PAGINA_ACTUAL_NO_ES_VALIDA);
        }
    }

    private void validarCantidadFilas(Integer cantidadFilas) {
        if (cantidadFilas == null) {
            throw new DataInvalidaException(LA_CANTIDAD_FILAS_NO_ES_VALIDA);
        }
    }

    private void calcularFilaInicial() {
        this.filaInicial = (paginaActual) * CANTIDAD_FILAS_PAGINA;
    }

    private void calcularFilaFinal(int cantidadFilasTotal) {

        int cantidadFilasPaginacion = this.filaInicial + CANTIDAD_FILAS_PAGINA;
        if (cantidadFilasPaginacion > cantidadFilasTotal) {
            cantidadFilasPaginacion = cantidadFilasTotal;
        }
        this.filaFinal = cantidadFilasPaginacion;

    }


}
