package com.apirestcidenet.dominio.excepcion;

public class DataInvalidaException extends RuntimeException {
    public DataInvalidaException(String mensaje) {
        super(mensaje);
    }
}
