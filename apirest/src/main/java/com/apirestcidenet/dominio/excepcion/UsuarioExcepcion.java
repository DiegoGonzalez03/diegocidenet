package com.apirestcidenet.dominio.excepcion;

public class UsuarioExcepcion extends RuntimeException {
    public UsuarioExcepcion(String mensaje) { super(mensaje);
    }
}
