package com.apirestcidenet.dominio.puerto;

import com.apirestcidenet.dominio.Paginacion;
import com.apirestcidenet.dominio.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsuarioPuerto {

    void crearUsuario(Usuario usuario);
    int correosIguales(String primerNombre,String primerApellido);
    Boolean buscarUsuarioPorTipoYNumeroIdentificacion(String tipo,String numero);
    int contar();
    List<Usuario> paginar(Paginacion paginacion);
    void eliminarUsuario(Usuario usuario);
    Usuario buscarPorId(Long id);
    void ActualizarUsuario(Usuario usuario);
}
