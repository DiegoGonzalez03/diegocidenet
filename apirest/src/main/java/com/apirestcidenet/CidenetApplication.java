package com.apirestcidenet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CidenetApplication {

    public static void main(String[] args) {
        SpringApplication.run(CidenetApplication.class, args);
    }

}
