package com.apirestcidenet.aplicacion.transformador;

import com.apirestcidenet.aplicacion.dto.UsuarioDto;
import com.apirestcidenet.dominio.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UsuarioTransformador {

    public static Usuario crearUsuario(UsuarioDto usuarioDto){
        Usuario usuario= new Usuario(usuarioDto.getPrimerApellido(), usuarioDto.getSegundoApellido(), usuarioDto.getPrimerNombre(), usuarioDto.getOtrosNombres()
        , usuarioDto.getPais(), usuarioDto.getTipoIdentificacion(), usuarioDto.getNumeroIdentificacion(), usuarioDto.getArea(), usuarioDto.getFechaIngreso());
            usuario.setFechaRegistro(usuarioDto.getFechaRegistro());
            usuario.setCorreo(usuarioDto.getCorreo());
            usuario.setEstado(usuarioDto.getEstado().equalsIgnoreCase("activo"));
            usuario.setId(usuarioDto.getId());
            usuario.setFechaEdicion(usuarioDto.getFechaEdicion());
            return  usuario;
    }


    public static UsuarioDto crearUsuarioDto(Usuario usuario){
        UsuarioDto usuarioDto = new UsuarioDto(usuario.getPrimerApellido(), usuario.getSegundoApellido(), usuario.getPrimerNombre(),  usuario.getOtrosNombres(),
                usuario.getPais(), usuario.getTipoIdentificacion(), usuario.getNumeroIdentificacion(),usuario.getArea());
        usuarioDto.setCorreo(usuario.getCorreo());
        usuarioDto.setFechaRegistro(usuario.getFechaRegistro());
        usuarioDto.setEstado(usuario.getEstado()?"activo":"inactivo");
        usuarioDto.setId(usuario.getId());
        usuarioDto.setFechaIngreso(usuario.getFechaIngreso());
        if(usuario.getFechaEdicion()!=null) usuarioDto.setFechaEdicion(usuario.getFechaEdicion());
        return usuarioDto;

    }
    public static List<UsuarioDto> crearListaUsuarioDtos(List<Usuario> usuarios){
        List<UsuarioDto> usuarioDtos =new ArrayList<>();
        usuarios.forEach((final Usuario usuario)-> usuarioDtos.add(crearUsuarioDto(usuario)));
        return usuarioDtos;
    }
}
