package com.apirestcidenet.aplicacion.manejador;

import java.util.List;

import org.springframework.stereotype.Component;

import com.apirestcidenet.aplicacion.dto.PaginacionUsuarioDto;
import com.apirestcidenet.aplicacion.dto.UsuarioDto;
import com.apirestcidenet.aplicacion.transformador.UsuarioTransformador;
import com.apirestcidenet.dominio.Paginacion;
import com.apirestcidenet.dominio.Usuario;
import com.apirestcidenet.dominio.servicio.ServicioPaginarUsuario;

@Component
public class ManejadorPaginarUsuario {

    private ServicioPaginarUsuario servicioPaginarUsuario;

    public ManejadorPaginarUsuario(ServicioPaginarUsuario servicioPaginarUsuario) {
        this.servicioPaginarUsuario = servicioPaginarUsuario;
    }

    public PaginacionUsuarioDto paginar(Integer paginaActual) {
        Paginacion paginacion = new Paginacion(paginaActual);
        List<Usuario> usuarios = this.servicioPaginarUsuario.listar(paginacion);
        return new PaginacionUsuarioDto(paginacion.getCantidadPaginas(), UsuarioTransformador.crearListaUsuarioDtos(usuarios));
    }

}
