package com.apirestcidenet.aplicacion.manejador;

import com.apirestcidenet.aplicacion.dto.UsuarioDto;
import com.apirestcidenet.aplicacion.transformador.UsuarioTransformador;
import com.apirestcidenet.dominio.servicio.ServicioCrearUsuario;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ManejadorCrearUsuario {

    private final ServicioCrearUsuario servicioCrearUsuario;


    public ManejadorCrearUsuario(ServicioCrearUsuario servicioCrearUsuario) {
        this.servicioCrearUsuario = servicioCrearUsuario;

    }
    @Transactional
    public String crearUsuario(UsuarioDto usuarioDto){
      return  this.servicioCrearUsuario.crearPago(UsuarioTransformador.crearUsuario(usuarioDto));

    }
}
