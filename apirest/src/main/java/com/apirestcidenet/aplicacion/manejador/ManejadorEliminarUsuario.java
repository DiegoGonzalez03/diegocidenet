package com.apirestcidenet.aplicacion.manejador;

import com.apirestcidenet.dominio.servicio.ServicioEliminarUsuario;
import org.springframework.stereotype.Component;

@Component
public class ManejadorEliminarUsuario {
    private final ServicioEliminarUsuario servicioEliminarUsuario;

    public ManejadorEliminarUsuario(ServicioEliminarUsuario servicioEliminarUsuario) {
        this.servicioEliminarUsuario = servicioEliminarUsuario;
    }
        public  String eliminarUsuario(Long id){
        return this.servicioEliminarUsuario.eliminarUsuario(id);
        }

}
