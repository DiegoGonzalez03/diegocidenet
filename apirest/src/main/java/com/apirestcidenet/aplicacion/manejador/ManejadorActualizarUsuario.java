package com.apirestcidenet.aplicacion.manejador;

import com.apirestcidenet.aplicacion.dto.UsuarioDto;
import com.apirestcidenet.aplicacion.transformador.UsuarioTransformador;
import com.apirestcidenet.dominio.servicio.ServicioActualizarUsuario;
import org.springframework.stereotype.Component;

@Component
public class ManejadorActualizarUsuario {
    private final ServicioActualizarUsuario servicioActualizarUsuario;

    public ManejadorActualizarUsuario(ServicioActualizarUsuario servicioActualizarUsuario) {
        this.servicioActualizarUsuario = servicioActualizarUsuario;
    }

    public  String actualizarUsuario(Long id, UsuarioDto usuarioDto){
        return  this.servicioActualizarUsuario.actualizarUsuario(id, UsuarioTransformador.crearUsuario(usuarioDto));
    }
}
