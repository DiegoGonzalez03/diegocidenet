package com.apirestcidenet.aplicacion.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDto {
    private String primerApellido;
    private String segundoApellido;
    private String primerNombre;
    private String OtrosNombres;
    private String pais;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String correo;
    private String fechaIngreso;
    private String area;
    private String estado;
    private String fechaRegistro;
    private Long id;
    private String fechaEdicion;
    public UsuarioDto(String primerApellido, String segudoApellido, String primerNombre, String otrosNombres, String pais, String tipoIdentificacion, String numeroIdentificacion, String area) {
        this.primerApellido = primerApellido;
        this.segundoApellido = segudoApellido;
        this.primerNombre = primerNombre;
        OtrosNombres = otrosNombres;
        this.pais = pais;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.fechaIngreso = fechaIngreso;
        this.area = area;
    }

    @Override
    public String toString() {
        return "UsuarioDto{" +
                "primerApellido='" + primerApellido + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", primerNombre='" + primerNombre + '\'' +
                ", OtrosNombres='" + OtrosNombres + '\'' +
                ", pais='" + pais + '\'' +
                ", tipoIdentificacion='" + tipoIdentificacion + '\'' +
                ", numeroIdentificacion='" + numeroIdentificacion + '\'' +
                ", correo='" + correo + '\'' +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", area='" + area + '\'' +
                ", estado='" + estado + '\'' +
                ", fechaRegistro='" + fechaRegistro + '\'' +
                ", id=" + id +
                ", fechaEdicion='" + fechaEdicion + '\'' +
                '}';
    }
}
