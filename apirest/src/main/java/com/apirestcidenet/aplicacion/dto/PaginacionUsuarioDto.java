package com.apirestcidenet.aplicacion.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class PaginacionUsuarioDto {
	private List<UsuarioDto> usuarios;
	private int numeroPaginas;
	
	public PaginacionUsuarioDto(int numeroPaginas, List<UsuarioDto> usuarios) {
		this.numeroPaginas = numeroPaginas;
		this.usuarios = usuarios;
	}
}
